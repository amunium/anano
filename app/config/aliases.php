<?php

/**
 * Shorthand aliases for commonly used classes.
 * Can also be used for inversion of control.
 */

return array(
    'App' => 'Anano\App',
    'Controller' => 'Anano\Controller',
    'Config' => 'Anano\Config',
    'Response' => 'Anano\Response\Response',
    'View' => 'Anano\Response\View',
    'Template' => 'Anano\Template',
    'Hash' => 'Anano\Crypto\Hash',
    'Input' => 'Anano\Input\Input',
    'Session' => 'Anano\Input\Session',
);