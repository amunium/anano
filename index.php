<?php

/**
 * Anano - Amunium Nano Framework.
 * 
 * Tiny framework inspired by several modern frameworks, primarily Laravel, but intended
 * to be extremely light-weight and fast, sacrificing advanced functionality and features
 * for speed and small memory footprint.
 *
 * @package Anano
 * @author  Kris Lux <email@amunium.dk>
 */

define('ROOT_DIR', __DIR__);

if (file_exists(__DIR__ . '/vendor/autoload.php'))
    require __DIR__ . '/vendor/autoload.php';

require __DIR__ . '/Anano/App.php';
require __DIR__ . '/Anano/Helpers.php';

\Anano\App::init();

$app = new App;