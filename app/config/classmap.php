<?php

/**
 * Folders to scan for classes that don't conform to PSR-0/PSR-4 structure.
 * You can transfer these to Composer's classmap if you prefer.
 */

return array(
    'classes',
    'controllers',
    'models',
);