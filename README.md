# README #

### What is it? ###

Anano, short for Amunium Nano framework, is intended as an extremely lightweight and fast replacement for heavier frameworks, such as Laravel, when most of their features are not needed. As such it should not be used for larger projects or anything you intend to support for longer periods.

### How to use ###

Clone the repo and have a look in the /app folder, most importantly /app/config, /app/controllers and /app/views. This should tell you what you need to know.